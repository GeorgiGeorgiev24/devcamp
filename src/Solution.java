
import java.util.InputMismatchException;
import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int m = 0;
        int n = 0;
        int brick = 0;
        try {
            m = in.nextInt();
            n = in.nextInt();
            System.out.println("M is " + m + " , N is " + n);
        } catch (InputMismatchException e) {
            System.out.println(e);
        }

        if (m <= 1 || m > 100 || m % 2 != 0) {
            System.out.println("M should be an even number between 2 and 100");
        } else if (n <= 1 || n > 100 || n % 2 != 0) {
            System.out.println("N should be an even number between 2 and 100");
        } else {
            int[][] a = new int[m][n];
            int[][] b = new int[m][n];
            int[] c = new int[m*n];
            int[] d = new int[m*n];
            for (int i = 0; i < m; i++) {
                for (int j = 0; j < n; j++) {
                    a[i][j] = in.nextInt();
                }
            }
            //2d array to 1d array
            int k = 0;
            for(int i = 0; i < m; i++) {
                for(int j = 0; j < n; j++) {
                    c[k] = a[i][j];
                    k++;
                }
            }
            for(int i = 1; i < m*n; i+=2) {
                brick++;
                if(c[i] == c[i-1]) {
                    //going on next line
                    boolean isSecondLine = true;
                    for(int j = 1; j <= m; j+=2) {
                        if(i > n * j && i < (j + 1) * n) {
                            d[i-1] = d[i-n-1];
                            d[i] = d[i-n];
                            brick--;
                            isSecondLine = false;
                            break;
                        }
                    }
                    if(isSecondLine) {
                        d[i-1] = brick;
                        brick++;
                        d[i] = brick;
                    }
                } else if(c[i] != c[i-1]) {
                    d[i-1] = brick;
                    d[i] = brick;
                }
            }

            System.out.println();

            //1d array to 2d array
            for(int i=0; i<m;i++) {
                for(int j=0;j<n;j++){
                    b[i][j] = d[j + i*n];
                }
            }
            for (int i = 0; i < m; i++) {
                for (int j = 0; j < n; j++) {
                    System.out.print("" + b[i][j]);
                }
                System.out.println();
            }
        }
    }
}